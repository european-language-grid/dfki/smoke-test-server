import json
import random
import time
from os import environ

import requests
from celery import Celery
from elg import Authentication, Catalog, Service
from elg.model import AudioRequest, TextRequest
from flask import Flask, jsonify, request, url_for
from loguru import logger
from prometheus_client import (CollectorRegistry, Counter, Enum, Gauge, Info,
                               Summary, push_to_gateway)
from tqdm import tqdm

app = Flask(__name__)

# Prometheus push gateway configuration
registry = CollectorRegistry()

# Celery configuration
app.config["CELERY_BROKER_URL"] = environ.get("CELERY_BROKER_URL")
app.config["CELERY_RESULT_BACKEND"] = environ.get("CELERY_RESULT_BACKEND")

# Initialize Celery
celery = Celery(app.name, broker=app.config["CELERY_BROKER_URL"])
celery.conf.update(app.config)

# Global parameters
DOMAIN = environ.get("DOMAIN") if environ.get("DOMAIN") != None else "live"
TIMEOUT = int(environ.get("SERVICE_TIMEOUT")) if environ.get("SERVICE_TIMEOUT") != None else 5 * 60
AUTH_FILE = "src/data/tokens.json"
SAMPLE_TXT_FILENAME = "src/data/test.txt"
SAMPLE_AUDIO_FILENAME = "src/data/test.mp3"

# Prometheus metrics
services_metrics = {
    "time": Summary(
        "request_processing_seconds",
        "Time spent processing request",
        ["id"],
        registry=registry,
    ),
    "success_counter": Counter("request_success", "HTTP Success", ["id"], registry=registry),
    "failure_counter": Counter("request_failures", "HTTP Success", ["id"], registry=registry),
    "state": Enum(
        "service_state",
        "last HTTP state",
        ["id"],
        states=["working", "not_working"],
        registry=registry,
    ),
    "info": Info("service_request_response", "last HTTP response", ["id"], registry=registry),
}

# Init id2name
id2name = {}

# Write tokens into tokens.json
ACCESS_TOKEN = environ.get("ACCESS_TOKEN")
REFRESH_TOKEN = environ.get("REFRESH_TOKEN")

logger.info("Starting the smoke test server with the following parameters")


def main():
    # Init metrics for all services
    init()


###########

#  Utils  #

###########

# Logic from https://gitlab.com/european-language-grid/ilsp/elg-catalogue-frontend-csr/-/blob/master/src/parsers/ServiceToolParser.js#L244
def get_media_type(record):
    media_type = "text"
    if record.service_info:
        if record.service_info.tool_type:
            if record.service_info.tool_type in ['Machine Translation', 'Information Extraction', 'Text-to-Speech Synthesis', 'Text categorization']:
                media_type = "text"
            elif record.service_info.tool_type in['Speech Recognition']:
                media_type = "audio"
    return media_type


# Run a service and raise an Error if it is not working
def test_run_lt(id, auth_file, sample_txt_filename, sample_audio_filename, round_number):
    """
    Test to run a LT with the Service class
    """
    logger.info(f"[{id}] Test service 👀")
    logger.debug(f"Round number: {round_number}")
    auth = Authentication.from_json(auth_file)
    auth.domain = f"https://{DOMAIN}.european-language-grid.eu"
    auth.access_token = ACCESS_TOKEN
    auth.refresh_token = REFRESH_TOKEN
    lt = Service.from_id(id=id, domain=DOMAIN, auth_object=auth, use_cache=False, cache_dir=None)
    sync_mode = False if round_number == 1 else True
    logger.debug(f"[{id}] Service initialized 👌")
    assert isinstance(lt, Service)
    params = {
        param["parameter_name"]: "random string" if "string" in param["parameter_type"] else None
        for param in lt.record["described_entity"]["lr_subclass"]["parameter"]
        if param["optional"] == False
    }
    logger.debug(f'{lt.record["described_entity"]["lr_subclass"]}')
    logger.debug(f'{lt.record["described_entity"]["lr_subclass"]["parameter"]}')
    logger.debug(f"Params: {params}")
    params = params if params != {} else None
    logger.debug(f"Params final: {params}")
    if get_media_type(lt.record) == "text":
        logger.debug(f"[{id}] text input")
        result = lt(
            TextRequest.from_file(sample_txt_filename, params=params),
            timeout=TIMEOUT * round_number,
            sync_mode=sync_mode,
        )
    elif get_media_type(lt.record) == "audio":
        logger.debug(f"[{id}] audio input")
        req = AudioRequest.from_file(sample_audio_filename, params=params)
        logger.debug(f"Req: {req}")
        result = lt(
            req,
            timeout=TIMEOUT * round_number,
            sync_mode=sync_mode,
        )
    else:
        raise ValueError(f"{lt.service_info} - Input type not implemented yet.")
    logger.debug(f"[{id}] Result: {result}")
    return result


# Run a service and update metrics according to the result
def run_service(id, round_number=1):
    if id not in id2name.keys():
        update_id2name()
    try:
        start = time.time()
        result = test_run_lt(id, AUTH_FILE, SAMPLE_TXT_FILENAME, SAMPLE_AUDIO_FILENAME, round_number)
        delay = time.time() - start
        services_metrics["time"].labels(id).observe(delay)
        services_metrics["success_counter"].labels(id).inc()
        services_metrics["failure_counter"].labels(id)
        services_metrics["state"].labels(id).state("working")
        services_metrics["info"].labels(id).info(
            {
                "result": str(result),
                "status_code": str(200),
                "service_name": id2name.get(id),
            }
        )
        push_to_gateway(environ.get("GATEWAY_URL"), job="run_service", registry=registry)
        logger.info(f"[{id}] Success")
        return {"result": result, "status_code": 200}
    except Exception as e:
        delay = time.time() - start
        services_metrics["time"].labels(id).observe(delay)
        services_metrics["success_counter"].labels(id)
        services_metrics["failure_counter"].labels(id).inc()
        services_metrics["state"].labels(id).state("not_working")
        services_metrics["info"].labels(id).info(
            {"result": str(e), "status_code": "error", "service_name": id2name.get(id)}
        )
        push_to_gateway(environ.get("GATEWAY_URL"), job="run_service", registry=registry)
        logger.info(f"[{id}] Failure")
        logger.error(f"[{id}] {e}")
        return {"error": str(e)}


def update_id2name(all_lt=None, limit=None):
    logger.info("Update id2name 👍")
    if all_lt is None:
        catalog = Catalog(domain=DOMAIN)
        all_lt = catalog.search(resource="Tool/Service", limit=limit if limit != None else 10000)
    global id2name
    id2name = {t.id: t.resource_name for t in all_lt}


# Get all services IDs
def get_all_lt_ids(limit=None):
    logger.info("Get all lt ids 👍")
    catalog = Catalog(domain=DOMAIN)
    all_lt = catalog.search(resource="Tool/Service", limit=limit if limit != None else 10000)
    all_lt_ids = [t.id for t in all_lt if t.elg_compatible_service]
    update_id2name(all_lt=all_lt)
    return all_lt_ids[:limit]


# Run one service
@celery.task(bind=True)
def run_one_service(self, id):
    self.update_state(state="PROGRESS", meta={"status": "Running service..."})
    output = run_service(id=id)
    return {"status": "Task completed!", "result": output}


# Run all the services
@celery.task(bind=True)
def run_all_services(self, limit):
    self.update_state(state="PROGRESS", meta={"current": 0, "total": 1, "status": "Getting ids..."})
    all_lt_ids = get_all_lt_ids(limit)
    self.update_state(
        state="PROGRESS",
        meta={
            "current": 0,
            "total": len(all_lt_ids),
            "status": "First round in progess...",
        },
    )
    not_working_ids = []
    for i, id in enumerate(tqdm(all_lt_ids, desc="⌛️ First round in progress...")):
        output = run_service(id=id)
        self.update_state(
            state="PROGRESS",
            meta={
                "current": i + 1,
                "total": len(all_lt_ids),
                "status": "First round in progess...",
            },
        )
        if "error" in output.keys():
            not_working_ids.append(id)
            logger.info(f"[{id}] not working added for the second round 🤞")
    self.update_state(
        state="PROGRESS",
        meta={
            "current": 0,
            "total": len(not_working_ids),
            "status": "Second round in progess...",
        },
    )
    still_not_working_ids = []
    logger.info(f"Start the second round for {len(not_working_ids)} services 🦾")
    for i, id in enumerate(tqdm(not_working_ids, desc="⏳ Second round in progress...")):
        output = run_service(id=id, round_number=2)
        self.update_state(
            state="PROGRESS",
            meta={
                "current": i + 1,
                "total": len(not_working_ids),
                "status": "Second round in progess...",
            },
        )
        if "error" in output.keys():
            still_not_working_ids.append(id)
            logger.info(f"[{id}] still not working 😟")

    return {
        "current": len(all_lt_ids),
        "total": len(all_lt_ids),
        "status": "Task completed!",
        "result": "finished",
    }


############

#  Routes  #

############


@app.route("/")
def home():
    return "Smoke tests server"


@app.route("/service/<id>")
def benchmark_one_service(id):
    id = int(id)
    task = run_one_service.apply_async(args=[id])
    return (
        jsonify({"Location": url_for("benchmark_one_service_status", task_id=task.id)}),
        202,
        {"Location": url_for("benchmark_one_service_status", task_id=task.id)},
    )


@app.route("/init")
def init():
    logger.info("Initialize the server")
    ids = get_all_lt_ids()
    logger.debug(f"Found {len(ids)} different services")
    for id in ids:
        if id not in id2name.keys():
            update_id2name()
        services_metrics["time"].labels(id)
        services_metrics["failure_counter"].labels(id)
        services_metrics["success_counter"].labels(id)
        services_metrics["state"].labels(id)
        services_metrics["info"].labels(id).info(
            {
                "result": "service not tested yet after init.",
                "status_code": "UNK",
                "service_name": id2name.get(id),
            }
        )
    push_to_gateway(environ.get("GATEWAY_URL"), job="run_service", registry=registry)
    logger.info("All the metrics are initialized")
    return "All the metrics are initialized"


@app.route("/service_status/<task_id>")
def benchmark_one_service_status(task_id):
    task = run_one_service.AsyncResult(task_id)
    if task.state == "PENDING":
        response = {"state": task.state, "status": "Pending..."}
    elif task.state != "FAILURE":
        response = {
            "state": task.state,
            "status": task.info.get("status", ""),
        }
        if "result" in task.info:
            response["result"] = task.info["result"]
    else:
        # something went wrong in the background job
        response = {
            "state": task.state,
            "status": str(task.info),  # this is the exception raised
        }
    return jsonify(response)


@app.route("/all_services")
def benchmark_all_services():
    limit = request.args.get("limit", default=10000, type=int)
    task = run_all_services.apply_async(args=[limit])
    return (
        jsonify({"Location": url_for("benchmark_all_services_status", task_id=task.id)}),
        202,
        {"Location": url_for("benchmark_all_services_status", task_id=task.id)},
    )


@app.route("/all_services_status/<task_id>")
def benchmark_all_services_status(task_id):
    task = run_all_services.AsyncResult(task_id)
    if task.state == "PENDING":
        response = {
            "state": task.state,
            "current": 0,
            "total": 1,
            "status": "Pending...",
        }
    elif task.state != "FAILURE":
        response = {
            "state": task.state,
            "current": task.info.get("current", 0),
            "total": task.info.get("total", 1),
            "status": task.info.get("status", ""),
        }
        if "result" in task.info:
            response["result"] = task.info["result"]
    else:
        # something went wrong in the background job
        response = {
            "state": task.state,
            "current": 1,
            "total": 1,
            "status": str(task.info),  # this is the exception raised
        }
    return jsonify(response)


main()
