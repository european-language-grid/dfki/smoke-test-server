# Smoke test server

## Description

To be done.

## Env variables to pass to the container

Variable | Info
--- | ---
``CELERY_BROKER_URL`` | url of the celery broker (redis://redis:6379/0)
``CELERY_RESULT_BACKEND`` | url of the celery result backend (redis://redis:6379/0)
``GATEWAY_URL`` | url of prometheuse gateway (gateway:9091)
``SERVICE_TIMEOUT`` | timeout on the service execution (in seconds, default 300)
``ACCESS_TOKEN`` | ELG access token
``REFRESH_TOKEN`` | ELG refresh token